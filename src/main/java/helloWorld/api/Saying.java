package helloWorld.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

/**
 * Created by a599250 on 21/06/2016.
 */
public class Saying {
    private long id;

    @Length(max = 3)
    private String contente;

    public Saying() {
    }

    public Saying(Long id, String contente) {
        this.id = id;
        this.contente = contente;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public String getContente() {
        return contente;
    }
}

package helloWorld;

import helloWorld.health.TemplateHealthCheck;
import helloWorld.resources.HelloWorldResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * Created by a599250 on 21/06/2016.
 */
public class HelloWorldApplication  extends Application<HelloWorldConfiguration>{

    public static void main(String[] args) throws Exception {
        new HelloWorldApplication().run(args);
    }

    public String getName() {
        return "hello-world";
    }

    public void initialize(Bootstrap<HelloWorldConfiguration> bootstrap){

    }

    public void run(HelloWorldConfiguration configuration, Environment environment)  {
        final HelloWorldResource resource = new HelloWorldResource(
                configuration.getTemplate(),
                configuration.getDefaultName()
        );
        environment.jersey().register(resource);

        final TemplateHealthCheck healthCheck =
                new TemplateHealthCheck(configuration.getTemplate());
        environment.healthChecks().register("template", healthCheck);
        environment.jersey().register(resource);

    }
}
